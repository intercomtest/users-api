'use strict';

// Core Modules
const bodyParser = require('body-parser');
const morgan = require('morgan');
const cookieParser = require('cookie-parser');
const compression = require('compression');

module.exports = function (express, app) {
  // Core Modules
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({
    extended: false
  }));
  app.use(cookieParser());
  app.use(compression());
  app.use(morgan('dev'));
};
