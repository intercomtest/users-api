'use strict';

const geometryUtil = require('../utils/geometryUtil');
const users = require('../data/users').users;

module.exports = {
  getUsersByDistance
};

function getUsersByDistance(model) {
  const usersWithinRadius = [];

  users.forEach((u) => {
    const distance = geometryUtil.getDistanceBetweenPoints(model.baseCoordinates, { latitude: u.latitude, longitude: u.longitude }, model.units);
    if (distance <= model.maxDistance) {
      usersWithinRadius.push(u);
    }
  });

  return usersWithinRadius;
}

