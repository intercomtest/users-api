'use strict';

const config = require('config');
const fs = require('fs');
const contents = JSON.parse(fs.readFileSync('./package.json'));
const logLevels = {
  ERROR: 'Error',
  INFO: 'Info'
};
const logger = {
  error: (message, data) => {
    _log(message, data, logLevels.ERROR);
  },
  info: (message, data) => {
    _log(message, data, logLevels.INFO);
  }
};

function _log(message, data) {
  /* eslint-disable no-console */
  console.log({
    data: data,
    env: config.logger.environment,
    group: config.logger.groupName,
    message: message,
    service: config.logger.serviceName,
    version: contents.version
  });
}

module.exports = logger;
