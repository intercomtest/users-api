'use strict';

const EARTH_RADIUS_KM = 6371;
const EARTH_RADIUS_MILES = 3959;
const distanceUnits = require('../enums/units');

module.exports = {
  getDistanceBetweenPoints
};

function getDistanceBetweenPoints(pointA, pointB, units) {
  const r = (units === distanceUnits.MILES) ? EARTH_RADIUS_MILES : EARTH_RADIUS_KM;
  const lat1 = _degreesToRadians(pointA.latitude);
  const lng1 = _degreesToRadians(pointA.longitude);
  const lat2 = _degreesToRadians(pointB.latitude);
  const lng2 = _degreesToRadians(pointB.longitude);
  const dLong = Math.abs(lng1 - lng2);
  const centralAngle = Math.acos((Math.sin(lat1) * Math.sin(lat2)) + (Math.cos(lat1) * Math.cos(lat2) * Math.cos(dLong)));

  return r * centralAngle;
}

function _degreesToRadians(deg) {
  return deg * (Math.PI / 180);
}
