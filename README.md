# README #

### Overview ###

This is a Node.js API project. It currently contains a single endpoint that will return the users within 100km of the Intercom office sorted by ascending userId.

The app is deployed live via docker and kubernetes within Azure and can be reached at http://intercom.seanenright.tech/users if needed.

### User Endpoint ###

Issuing a GET Request to /user will return the required data specified within the coding test.

To reflect reality there are optional params that can also be used, in full to reflect the requirements of the test these would be /user?lat=53.339428&long=-6.257664&maxDistance=100&units=km

### How do I get set up? ###

Clone the repo.
Execute npm install in the working directory.
Execute npm start to launch the app on port 3000.
Issue a request to http://localhost:3000/user to retrieve the user list.

### Linting ###

The app is linted using grunt and eslint.
To validate execute: grunt eslint:validate.
To fix execute: grunt eslint:fix.

### Unit Tests ###

The app is fully unit tested using the Mocha framework and nyc test coverage.

Execute npm test within the directory to run the unit tests and validate test coverage.

### Jenkins ###

http://gtjnk.westeurope.cloudapp.azure.com:8080/view/users-api/