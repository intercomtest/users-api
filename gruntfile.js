'use strict';

module.exports = function (grunt) {
  /* eslint-disable global-require, import/no-extraneous-dependencies */
  require('load-grunt-tasks')(grunt);

  const lintFiles = [
    'controllers/**/*.js',
    'middleware/**/*.js',
    'utils/**/*.js',
    'test/**/*.js',
    'test-integration/**/*.js',
    'delegates/**/*.js',
    'routes/**/*.js',
    'services/**/*.js',
    'mappers/**/*.js',
    'helpers/**/*.js',
    'clients/**/*.js',
    'clientWrappers/**/*.js',
    'repositories/**/*.js',
    './gruntfile.js',
    './app.js',
    'public/scripts/app/*.js'
  ];

  grunt.initConfig({

    nodemon: {
      script: 'app.js',
      options: {
        ext: 'js',
        ignore: ['node_modules/**', 'coverage/**', 'test/**', 'gruntfile.js'],
        env: { NODE_ENV: 'development' }
      }
    },

    eslint: {
      validate: {
        options: {
          fix: false
        },
        src: lintFiles
      },
      fix: {
        options: {
          fix: true
        },
        src: lintFiles
      }
    },

    watch: {
      options: {
        livereload: true
      },
      jshint: {
        files: ['public/scripts/**/*.js'],
        tasks: ['jshint:all']
      }
    }
  });

  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-nodemon');

  // Default task(s).
  grunt.registerTask('default', ['jshint', 'watch']);

  // Go tasks
  grunt.registerTask('build', ['jshint']);
  grunt.registerTask('start', ['nodemon']);
};
