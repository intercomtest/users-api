'use strict';

const config = require('config');
const express = require('express');
const app = express();
const middlewareConfig = require('./middleware/middlewareConfig.js');
const routeConfig = require('./routes/routeConfig');
const logger = require('./utils/logger');

require('dnscache')(config.dnsCache);

middlewareConfig(express, app);
routeConfig(app);

const server = app.listen(config.port, () => {
  /* eslint-disable global-require, import/no-extraneous-dependencies */
  logger.info(`Listening on port ${config.port} ${Date()}`);
});

process.on('SIGTERM', () => {
  server.close(() => {
    logger.info('SIGTERM Received', null, 'Self', null, null, null, null);
    process.exit(0);
  });
});

process.on('uncaughtException', (err) => {
  logger.fatal('Uncaught Exception', err, 'Self', null, null, null, null);
  process.exit(1);
});
