env.GROUP_NAME='' // Example: share
env.STACK_NAME='' // Example: webpart
env.STACK_NAMESPACE='' // Example: share-webpart
env.KUBE_FILE_NAME='' // Example: webpart
env.REPOSITORY_PATH='' // Example: share/webpart
env.KUBE_TOKEN=''
env.CDN_TOKEN_FOLDER='' // API does not have UI scripts

env.sonarProjectKey='' // Example: Sharing:Share.Webpart
env.sonarProjectName='' // Example: Share.Webpart
// Default (can be modified)
env.sonarBranch='master' 
env.sonarExclusions='test/**,test-integration/**,node_modules/**,build/**,config/**,resources/**,coverage/**,app.js,gruntfile.js,newrelic.js,lib/data/**,public/assets/**,public/scripts/app_tests_*/**,public/scripts/libs/**,public/scripts/js/**,public/scripts/app/vendor/**,public/scripts/app_js/out/**,karma.conf.js'
env.sonarLanguage='js'
