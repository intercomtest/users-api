'use strict';

const express = require('express');
const router = express.Router();
const indexController = require('../controllers/userController');

router.get('/', indexController.get);

module.exports = router;
