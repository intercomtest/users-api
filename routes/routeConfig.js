'use strict';

const httpStatus = require('http-status');
const userRoute = require('./user');

module.exports = function (app) {
  app.use('/healthcheck', (req, res) => {
    res.status(httpStatus.OK).send('I am healthy!');
  });

  app.use('/user', userRoute);

  // 404 Forward
  app.use((req, res, next) => {
    const err = new Error('Not Found');
    err.status = 404;
    next(err);
  });

  // Production Handler
  app.use((err, req, res) => {
    res.status(err.status || httpStatus.INTERNAL_SERVER_ERROR).send(err.message);
  });
};
