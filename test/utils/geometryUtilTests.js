'use strict';

const expect = require('chai').expect;
const geometryUtil = require('../../utils/geometryUtil');

describe('geometryUtil', () => {
  let testPointA;
  let testPointB;
  let kmExpectation;
  let mileExpectation;

  beforeEach(() => {
    testPointA = { latitude: 53.339428, longitude: -6.257664 };
    testPointB = { latitude: 52.986375, longitude: -6.043701 };
    kmExpectation = 41;
    mileExpectation = 25;
  });

  describe('getDistanceBetweenPoints', () => {
    it('should calculate distance in kilometres', () => {
      const result = geometryUtil.getDistanceBetweenPoints(testPointA, testPointB, 'km');
      expect(parseInt(result)).to.equal(parseInt(kmExpectation));
    });

    it('should calculate distance in miles', () => {
      const result = geometryUtil.getDistanceBetweenPoints(testPointA, testPointB, 'miles');
      expect(parseInt(result)).to.equal(parseInt(mileExpectation));
    });
  });
});
