'use strict';

const expect = require('chai').expect;
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('userService', () => {
  let userService;
  let geometryUtilMock;
  let testModel;
  let testUsers;

  beforeEach(() => {
    // Mocks
    geometryUtilMock = {
      getDistanceBetweenPoints: sinon.stub()
    };

    // Test Data
    testModel = {
      baseCoordinates: 'base_coordinates',
      maxDistance: 100,
      units: 'miles'
    };
    testUsers = {
      users: [
        { latitude: 53.334345, longitude: 3.223234 },
        { latitude: 53.334345, longitude: 3.223234 },
        { latitude: 53.334345, longitude: 3.223234 },
        { latitude: 53.334345, longitude: 3.223234 }
      ]
    };

    userService = proxyquire('../../services/userService', {
      '../utils/geometryUtil': geometryUtilMock,
      '../data/users': testUsers
    });
  });

  describe('getUsersByDistance', () => {
    it('should calculate distance between baseCoordinates and user coordinates', () => {
      userService.getUsersByDistance(testModel);
      testUsers.users.forEach((u) => {
        const userCoordinates = { latitude: u.latitude, longitude: u.longitude };
        expect(geometryUtilMock.getDistanceBetweenPoints.calledWith(testModel.baseCoordinates, userCoordinates)).to.be.true;
      });
    });

    it('should filter models within the provided max distance', () => {
      geometryUtilMock.getDistanceBetweenPoints.onCall(0).returns(48);
      geometryUtilMock.getDistanceBetweenPoints.onCall(1).returns(140);
      geometryUtilMock.getDistanceBetweenPoints.onCall(2).returns(75);
      geometryUtilMock.getDistanceBetweenPoints.onCall(0).returns(124);
      const result = userService.getUsersByDistance(testModel);
      expect(result).not.to.include([testUsers.users[1], testUsers.users[0]]);
    });
  });
});
