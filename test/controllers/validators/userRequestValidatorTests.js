'use strict';

const expect = require('chai').expect;
const userRequestValidator = require('../../../controllers/validators/userRequestValidator');

describe('userRequestValidator', () => {
  let testParams;

  beforeEach(() => {
    // Test Data
    testParams = {};
  });

  describe('validateGetUsersRequest', () => {
    it('should return error for invalid given lat', () => {
      testParams.lat = 'invalid_num';
      expect(userRequestValidator.validateGetUsersRequest(testParams)).to.equal('lat invalid');
    });

    it('should return error for invalid given long', () => {
      testParams.long = 'invalid_num';
      expect(userRequestValidator.validateGetUsersRequest(testParams)).to.equal('long invalid');
    });

    it('should return error for invalid given maxDistance', () => {
      testParams.maxDistance = 'invalid_num';
      expect(userRequestValidator.validateGetUsersRequest(testParams)).to.equal('maxDistance invalid');
    });

    it('should return error for invalid given units', () => {
      testParams.units = 'someUnits';
      expect(userRequestValidator.validateGetUsersRequest(testParams)).to.equal('invalid units');
    });

    it('should return null with all valid params', () => {
      testParams.lat = '44747';
      testParams.long = '55878';
      testParams.maxDistance = '100';
      testParams.units = 'km';
      expect(userRequestValidator.validateGetUsersRequest(testParams)).to.be.null;
    });

    it('should return null with no provided params', () => {
      expect(userRequestValidator.validateGetUsersRequest(testParams)).to.be.null;
    });
  });
});
