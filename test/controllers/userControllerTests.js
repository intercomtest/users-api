'use strict';

const expect = require('chai').expect;
const sinon = require('sinon');
const proxyquire = require('proxyquire').noCallThru();

describe('userController', () => {
  let loggerMock;
  let userController;
  let testRequest;
  let testResponse;
  let requestMapperMock;
  let responseMapperMock;
  let validatorMock;
  let userServiceMock;

  beforeEach(() => {
    // Mocks
    testRequest = {
      query: { lat: 99898 }
    };
    testResponse = {
      status: sinon.stub().returnsThis(),
      send: sinon.stub()
    };
    requestMapperMock = {
      mapGetUsersRequest: sinon.stub()
    };
    responseMapperMock = {
      mapGetUsersResponse: sinon.stub()
    };
    userServiceMock = {
      getUsersByDistance: sinon.stub()
    };
    validatorMock = {
      validateGetUsersRequest: sinon.stub()
    };
    loggerMock = {
      error: sinon.stub()
    };

    // Setup
    userController = proxyquire('../../controllers/userController', {
      './mappers/userRequestMapper': requestMapperMock,
      './validators/userRequestValidator': validatorMock,
      './mappers/userResponseMapper': responseMapperMock,
      '../services/userService': userServiceMock,
      '../utils/logger': loggerMock
    });
  });

  describe('GET', () => {
    it('should return bad request error if validation error detected', () => {
      const validationError = 'some error';
      validatorMock.validateGetUsersRequest.returns(validationError);
      userController.get(testRequest, testResponse);
      expect(testResponse.status.calledWith(400)).to.be.true;
      expect(testResponse.send.calledWith(validationError)).to.be.true;
    });

    it('should map incoming query model', () => {
      userController.get(testRequest, testResponse);
      expect(requestMapperMock.mapGetUsersRequest.calledWith(testRequest.query)).to.be.true;
    });

    it('should retrieve users with mapped request model', () => {
      const mappedResult = { maxDistance: 99 };
      requestMapperMock.mapGetUsersRequest.returns(mappedResult);
      userController.get(testRequest, testResponse);
      expect(userServiceMock.getUsersByDistance.calledWith(mappedResult)).to.be.true;
    });

    it('should map retrieved user models', (done) => {
      const usersResult = ['user1', 'user2'];
      userServiceMock.getUsersByDistance.returns(usersResult);
      testResponse.send = () => {
        expect(responseMapperMock.mapGetUsersResponse.calledWith(usersResult)).to.be.true;
        done();
      };
      userController.get(testRequest, testResponse);
    });

    it('should respond with mapped user models on success', (done) => {
      const mappedUsersResult = ['mapped1', 'mapped2'];
      responseMapperMock.mapGetUsersResponse.returns(mappedUsersResult);
      testResponse.send = (data) => {
        expect(data).to.equal(mappedUsersResult);
        done();
      };
      userController.get(testRequest, testResponse);
    });

    it('should log and respond with error on service failure', (done) => {
      const err = new Error('service failure');
      userServiceMock.getUsersByDistance.throws(err);
      testResponse.send = (data) => {
        expect(testResponse.status.calledWith(500)).to.be.true;
        expect(data).to.deep.equal(err);
        done();
      };
      userController.get(testRequest, testResponse);
    });
  });
});
