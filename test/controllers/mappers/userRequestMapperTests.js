'use strict';

const expect = require('chai').expect;
const proxyquire = require('proxyquire').noCallThru();
const units = require('../../../enums/units');

describe('userRequestMapper', () => {
  let configMock;
  let testParams;
  let userRequestMapper;

  beforeEach(() => {
    // Mocks
    configMock = {
      baseLocation: {
        latitude: 57.6658542,
        longitude: 4.3365
      },
      defaultMaxDistance: 100
    };

    // Test Data
    testParams = {
      lat: '53.11454',
      long: '2.33696',
      maxDistance: '58',
      units: 'MILES'
    };

    // Setup
    userRequestMapper = proxyquire('../../../controllers/mappers/userRequestMapper', {
      config: configMock
    });
  });

  describe('mapGetUsersRequest', () => {
    it('should map default baseCoordinates.latitude if not provided', () => {
      delete testParams.lat;
      const result = userRequestMapper.mapGetUsersRequest(testParams);
      expect(result.baseCoordinates).to.deep.equal({
        latitude: configMock.baseLocation.latitude,
        longitude: parseFloat(testParams.long)
      });
    });

    it('should map default baseCoordinates.longitude if not provided', () => {
      delete testParams.long;
      const result = userRequestMapper.mapGetUsersRequest(testParams);
      expect(result.baseCoordinates).to.deep.equal({
        latitude: parseFloat(testParams.lat),
        longitude: configMock.baseLocation.longitude
      });
    });

    it('should map maxDistance if provided', () => {
      const result = userRequestMapper.mapGetUsersRequest(testParams);
      expect(result.maxDistance).to.equal(parseFloat(testParams.maxDistance));
    });

    it('should use default maxDistance if none provided', () => {
      delete testParams.maxDistance;
      const result = userRequestMapper.mapGetUsersRequest(testParams);
      expect(result.maxDistance).to.equal(configMock.defaultMaxDistance);
    });

    it('should map units if provided', () => {
      const result = userRequestMapper.mapGetUsersRequest(testParams);
      expect(result.units).to.equal(units.MILES);
    });

    it('should map default km units if none specified', () => {
      delete testParams.units;
      const result = userRequestMapper.mapGetUsersRequest(testParams);
      expect(result.units).to.equal(units.KM);
    });
  });
});
