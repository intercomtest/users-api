'use strict';

const expect = require('chai').expect;
const userResponseMapper = require('../../../controllers/mappers/userResponseMapper');

describe('userResponseMapper', () => {
  let testUsers;

  beforeEach(() => {
    testUsers = [
      {
        name: 'Eddard Stark',
        user_id: 87
      },
      {
        name: 'Jaime Lannister',
        user_id: 21
      },
      {
        name: 'Tywin Lannister',
        user_id: 101
      }
    ];
  });

  describe('mapGetUsersResponse', () => {
    it('should map user model', () => {
      const result = userResponseMapper.mapGetUsersResponse(testUsers);
      expect(result[0]).to.have.all.keys(['name', 'userId']);
    });

    it('should order users by user ids ascending', () => {
      const result = userResponseMapper.mapGetUsersResponse(testUsers);
      expect(result).to.deep.equal([
        { name: 'Jaime Lannister', userId: 21 },
        { name: 'Eddard Stark', userId: 87 },
        { name: 'Tywin Lannister', userId: 101 }
      ]);
    });
  });
});
