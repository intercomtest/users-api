'use strict';

const units = require('../../enums/units');

module.exports = {
  validateGetUsersRequest
};

function validateGetUsersRequest(queryParams) {
  const numberParams = ['lat', 'long', 'maxDistance'];

  for (let i = 0; i < numberParams.length; i++) {
    const param = numberParams[i];
    if (queryParams[param]) {
      const parsedVal = parseFloat(queryParams[param]);
      if (Number.isNaN(parsedVal)) {
        return `${param} invalid`;
      }
    }
  }

  if (queryParams.units && !units[queryParams.units.toUpperCase()]) {
    return 'invalid units';
  }

  return null;
}
