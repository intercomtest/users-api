'use strict';

const httpStatus = require('http-status');
const logger = require('../utils/logger');
const userRequestMapper = require('./mappers/userRequestMapper');
const userResponseMapper = require('./mappers/userResponseMapper');
const userRequestValidator = require('./validators/userRequestValidator');
const userService = require('../services/userService');

module.exports = {
  get: _get
};

function _get(req, res) {
  const validationError = userRequestValidator.validateGetUsersRequest(req.query);

  if (validationError) {
    return res.status(httpStatus.BAD_REQUEST).send(validationError);
  }

  try {
    const mappedRequestModel = userRequestMapper.mapGetUsersRequest(req.query);
    const users = userService.getUsersByDistance(mappedRequestModel);
    res.send(userResponseMapper.mapGetUsersResponse(users));
  } catch (error) {
    logger.error('GET Users', error);
    res.status(httpStatus.INTERNAL_SERVER_ERROR).send(error);
  }
}
