'use strict';

module.exports = {
  mapGetUsersResponse
};

function mapGetUsersResponse(users) {
  users.sort((x, y) => x.user_id - y.user_id);
  return users.map((u) => ({
    name: u.name,
    userId: u.user_id
  }));
}
