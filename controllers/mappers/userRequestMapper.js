'use strict';

const config = require('config');
const units = require('../../enums/units');

module.exports = {
  mapGetUsersRequest
};

function mapGetUsersRequest(queryParams) {
  return {
    baseCoordinates: {
      latitude: queryParams.lat ? parseFloat(queryParams.lat) : config.baseLocation.latitude,
      longitude: queryParams.long ? parseFloat(queryParams.long) : config.baseLocation.longitude
    },
    maxDistance: queryParams.maxDistance ? parseFloat(queryParams.maxDistance) : config.defaultMaxDistance,
    units: queryParams.units ? units[queryParams.units.toUpperCase()] : units.KM
  };
}
